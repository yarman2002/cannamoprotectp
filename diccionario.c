#include "diccionario.h"

#include <stdlib.h>
#include <stdio.h>
/*
struct elemento crear_elemento(char* llave, int tamano_llave, int* valor, int tamano_valor){
    struct elemento* elemento_a_crear= calloc(1,sizeof(struct elemento));
    elemento_a_crear->llave=llave;
    elemento_a_crear->valor=valor;
}
*/

int verificar_redimensionar(struct diccionario* tabla){
    float tamanno=tabla->tamanno;
    float llenura=tabla->llenura;
    float porcentaje_llenura=llenura/tamanno;
    //printf("%f \n",porcentaje_llenura);
    if((porcentaje_llenura>0.74)){
        return 1;
    }
    return 0;
}

void imprimir_nodo(struct elemento* elemento_a_imprimir){
    struct elemento* auxiliar=elemento_a_imprimir;
    while (auxiliar!=NULL)
    {
        printf("Llave: %s  Valor: %d \n",auxiliar->llave,auxiliar->valor);
        auxiliar=auxiliar->siguiente;
    }

    
    

}
void imprimir_diccionario(struct diccionario* diccionario_imprimir){
    struct elemento** lista=diccionario_imprimir->tabla_hash;
    int tamanno=diccionario_imprimir->tamanno;
    for (int i=0;i<tamanno;i++){
        printf("Posicion %d \n",i);
        if (lista[i]==NULL)
        {
            printf("NULL \n");
        }
        
        imprimir_nodo(lista[i]);
    }
    
}
void* relocalizar_nodo(struct diccionario* diccionario,struct elemento* nodo){
    struct elemento* auxiliar=nodo;
    struct elemento* siguiente=auxiliar->siguiente;
    int nueva_llave;
    int tamanno_diccionario=diccionario->tamanno;
    while (siguiente!=NULL)
    {
        nueva_llave=hashing_function(tamanno_diccionario,auxiliar->llave);
        insertar_elemento(nueva_llave,auxiliar->valor,auxiliar->llave,diccionario);
        free(auxiliar);
        auxiliar=siguiente;
        siguiente=siguiente->siguiente;

    }
    nueva_llave=hashing_function(tamanno_diccionario,auxiliar->llave);
        insertar_elemento(nueva_llave,auxiliar->valor,auxiliar->llave,diccionario);
        free(auxiliar);
    


}
void* relocalizar_elementos(struct diccionario* diccionario_viejo, struct diccionario* diccionario_nuevo){
    int tamanno_arreglo_viejo=diccionario_viejo->tamanno;
    struct elemento** tabla_vieja=diccionario_viejo->tabla_hash;
    for(int i=0;i<tamanno_arreglo_viejo;i++){
        if (tabla_vieja[i]!=NULL){
            relocalizar_nodo(diccionario_nuevo,tabla_vieja[i]);
        }

    }
    
    
    

}
struct diccionario* redimensionar_tabla(struct diccionario* tabla_vieja){
    int nuevo_tamanno=siguiente_primo(tabla_vieja->tamanno*2);
    struct diccionario* nuevo_diccionario=calloc(1,sizeof(struct diccionario));
    nuevo_diccionario->tabla_hash=calloc(nuevo_tamanno,sizeof(struct elemento*));
    nuevo_diccionario->tamanno=nuevo_tamanno;
    nuevo_diccionario->llenura=0;
    relocalizar_elementos(tabla_vieja,nuevo_diccionario);
    free(tabla_vieja->tabla_hash);
    free(tabla_vieja);
    return nuevo_diccionario;
}
int buscar_en_tabla(char* llave,struct diccionario* tabla){
    int posicion=hashing_function(tabla->tamanno,llave);
    struct elemento* elemento_a_revisar=tabla->tabla_hash[posicion];
    while (elemento_a_revisar!=NULL)
    {
        if (elemento_a_revisar->llave==llave){
            return elemento_a_revisar->valor;
        }
        elemento_a_revisar=elemento_a_revisar->siguiente;
    }
    return 0;
    

}

void insertar_elemento(int llave_hash,int valor,char* llave,struct diccionario* tabla){
    
    tabla->llenura++;
    struct elemento* elemento_a_insertar = calloc(1,sizeof(struct elemento));
    elemento_a_insertar->llave=llave;
    elemento_a_insertar->valor=valor;

    if (tabla->tabla_hash[llave_hash]==NULL)
    {
        tabla->tabla_hash[llave_hash]=elemento_a_insertar;
        return;
    }
    struct elemento* auxiliar=tabla->tabla_hash[llave_hash];
    while (auxiliar->siguiente!=NULL)
    {
        auxiliar=auxiliar->siguiente;
    }
    auxiliar->siguiente=elemento_a_insertar;
    
    
    
}

struct diccionario* crear_tabla_hash(int tamanno){
    struct diccionario* diccionario_a_crear= calloc(1,sizeof(struct diccionario));
    diccionario_a_crear->tabla_hash=calloc(tamanno,sizeof(struct elemento*));
    diccionario_a_crear->tamanno=tamanno;
    diccionario_a_crear->llenura=0;
    //struct diccionario** tabla_hash=calloc(7,sizeof(struct elemento*));
    return diccionario_a_crear;
}
/*
void insertar_al_final(struct nodo_lista* primer_nodo,char* valor_a_agregar){
    struct nodo_lista* actual=primer_nodo;
    if (actual->valor==NULL){
        actual->valor=valor_a_agregar;
        return;
    }
    while (actual->siguiente!=NULL)
    {
        actual=actual->siguiente;
    }
    actual -> siguiente= calloc(1,sizeof(struct nodo_lista));
    actual = actual->siguiente;
    actual->valor=valor_a_agregar;
    
    
    
}
*/
int hashing_function(int tamanno_arreglo,char * llave_a_convertir){
    int caracter=llave_a_convertir[0];
    int sumatoria=0;
    int contador=0;
    while (caracter)
    {
        sumatoria+=caracter;
        contador++;
        caracter=llave_a_convertir[contador];
    }
    sumatoria=sumatoria%tamanno_arreglo;
    return sumatoria;

}
int es_primo(int n){
    if ( n <= 1)
    {
        return 0;
    }
    if (n <= 3)
    {
        return 1;
    }
    if (n%2 ==0 || n%3 ==0){
        return 0;
    }
    for (int i=5; i*i<=n; i=i+6){
        if (n%i==0 || n%(i+2)==0){
            return 0;
        }
    }
    return 1;

    
    
}
int siguiente_primo(int n){
    if (n<=1){
        return 2;
    }
    int numero_primo=n;
    int encontrado=0;
    while (!encontrado){
        numero_primo++;
        if (es_primo(numero_primo)){
            encontrado=1;
        }
    }
    return numero_primo;
}
