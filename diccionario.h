
#include <stdlib.h>
#include <stdio.h>
#ifndef diccionario_h
#define diccionario_h

struct diccionario{
    int llenura;
    int tamanno;
    struct elemento** tabla_hash;
};
struct nodo_lista{
    char* valor;
    struct nodo_lista* siguiente;
};
struct elemento{
    char* llave;
    int valor;
    struct elemento* siguiente;

};
// Busca un elemento en la tabla de hash
/* @param LLave El string llave del elemento a buscar
* @param tabla_hash La tabla en la que se busca el elemento
* @param tamanno El tamanno de la tabla
*/
int buscar_en_tabla(char* llave,struct diccionario* tabla);

// Crea una tabla de hash segun el tamanno que se le pase
struct diccionario* crear_tabla_hash(int tamanno);


/* Inserta un elemento en una tabla de hash
* @param llave_hash El entero que se obtiene al desencriptar la llave, osea la posicion del arreglo
* @param valor El valor del elemento
* @param llave La llave del elemento
* @param tabla_hash La tabla donde se insertarà el elemento.
*/
void insertar_elemento(int llave_hash,int valor,char* llave,struct diccionario* tabla);

/*
*
*/
//struct elemento crear_elemento(char* llave, int tamano_llave, int* valor, int tamano_valor);

/*
* @param llave_a_c#include <stdlib.h>
#include <stdio.h>
#ifndef diccionario_h
#define diccionario_honvertir El string que queremos convertir a una posicion 
*/
int hashing_function(int tamanno_arreglo,char * llave_a_convertir);

/*
* El primo que sigue despues de n.
*/
int siguiente_primo(int n);


int es_primo (int n);
//void insertar_al_final(struct nodo_lista* primer_nodo,char* valor_a_agregar);
/*
* Relocaliza cada elemento de un arreglo individualmente.
*/
void* relocalizar_nodo(struct diccionario* diccionario,struct elemento* nodo);
/*
* Transfiere los elementos de un diccionario a otro, no necesariamente quedan en las mismas posiciones.
* No necesariamente quedan los mismos elementos enlazados
*/
void* relocalizar_elementos(struct diccionario* diccionario_viejo, struct diccionario* diccionario_nuevo);

/*
* Crea una nueva tabla con los mismos elementos, pero con un tamanno mayor.
* El tamanno mayor definido es el siguiente primo, de multiplicar por 2 el tamanno actual.
*/
struct diccionario* redimensionar_tabla(struct diccionario* tabla_vieja);
void imprimir_nodo(struct elemento* elemento_a_imprimir);
void imprimir_diccionario(struct diccionario* diccionario_imprimir);
int verificar_redimensionar(struct diccionario* tabla);
#endif /* diccionario_h */

