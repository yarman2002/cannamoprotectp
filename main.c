

#include "diccionario.h"
#include "funciones_juego.h"
#include <stdlib.h>
#include <time.h>
#include<string.h>
#include <stdio.h>
#include <unistd.h>
#define clrscr() printf("\e[1;1H\e[2J")

/*
* @param Cantidad, la cantidad de ciudades a crear
*/
struct lista_ciudades* verificar_ciudades(int cantidad){
    struct lista_ciudades* ciudades;
    clrscr();
    if(cantidad<3){
        printf("Minimo superado, se crearan 3 ciudades\n");
        sleep(2);
         ciudades = crear_ciudades(3);

    }
    else if(cantidad>10){
        printf("Maximo superado, se crearan 10 ciudades\n");
        sleep(2);
         ciudades = crear_ciudades(10);
    }
    else{
         ciudades = crear_ciudades(cantidad);

    }
    printf("Se han creado las ciudades \n");
    sleep(1);
    return ciudades;
}
/*
* @param jugador el struct del jugador. Para obtener datos en la impresion
* @param ciudadess La lista de ciudades que se van a trabar
* @param Turno El turno actual
* Esta funcion maneja el menu de cada turno
* Return: 1 Si el usuario decide retirar de la partida
*/
int turno_jugador(struct jugador* jugador, struct lista_ciudades* ciudadess, int turno){
    int opcion;
    while (1)
    {
        printf("\n Tabla del jugador \n");
        printf("------------------------------------------------------------------------------------------\n");
        imprimir_tabla_jugador(jugador);
        printf("------------------------------------------------------------------------------------------\n");
        imprimir_ciudades(ciudadess);
        printf("\nTurno %d\n",turno);
        printf("Turnos restantes %d \n",25-turno);
        printf("-----------------------------------------------------------------------------------------");
        printf("\n Cantidad de Gil actual: %d\n" ,jugador->gil);
        printf(" Deuda: %d\n" ,jugador->deuda);
        printf("Gil necesarios para pagar la deuda: %d\n" ,(jugador->deuda-jugador->gil));
        printf("\n\nQue desea hacer?\n 1. Vender\n 2. Comprar \n 3. Hacer publicidad (20 gils)\n");
        printf(" 4. Reahacer su tabla (50 gil) \n 5. Nada \n 6. Terminar el juego\n");
        scanf(" %d",&opcion);
        if (opcion<1|| opcion>6){
            clrscr();
            printf("Opcion invalidad");
            continue;
        }
        if (opcion==1){
            if(vender(jugador,ciudadess)){
                clrscr();
                printf("Operacion cancelada\n");
                sleep(2);
                continue;
            }
            clrscr();
            
            break;
        }
        else if (opcion==2){
            if(comprar(jugador,ciudadess)){
                clrscr();
                printf("Operacion cancelada\n");
                sleep(2);

                continue;
            }
            clrscr();
            
            break;
        }
        else if (opcion==3){
            clrscr();
            if(hacer_publicidad(jugador,ciudadess)){
                clrscr();
                printf("Operacion cancelada, no hay suficiente gil.\n");
                sleep(2);

                
                continue;
            }
            clrscr();

            break;
        }
        else if (opcion==4){
            if((jugador->gil-50)<0){
                printf("No hay gil suficiente");
                sleep(2);

                clrscr();
                continue;
            }
            jugador->gil-=50;
            rehacer_tabla(jugador);
            clrscr();
            printf("Tabla rehecha \n");
            
            break;
        }
        else if (opcion==5){
            clrscr();
            
            printf("Turno saltado \n");
                sleep(2);

            
            break;
        }
        else{
            return 1; // Si la opcion es 6 entonces devuelve 1
        }
        
        

    }
    if (opcion!=3){
        
        foo(ciudadess);
        }
    
    
    return 0; // devuelve 0 para que no se active el condicional de salida
    
}
int main()
{
    printf("Bienvenido a Gangsta Paradise Beta 0.69\n");
    printf("Quiere ver las reglas? Y/N\n");
    char * reglas;
    scanf("%s",reglas);
    
    if(!strcmp(reglas,"Y")|| !strcmp(reglas,"y")){
        
        imprimir_reglas();
    }
    printf("Digite la cantidad de ciudades con las que desea empresar.\nTome en cuenta que tiene que ser un valor entre 3 y 10 \n");
    int cantidad_ciudades=0; int turno=1;
    scanf("%d",&cantidad_ciudades);
    srand(time(NULL));
    int randomnumber;
    randomnumber = rand() % 10;
    struct jugador* jugador= calloc(1,sizeof(struct jugador));
    crear_tabla_derivados_jugador(jugador);
    jugador->gil=100;
    jugador->deuda=5000;
    

    //int nRandonNumber = rand()%((nMax+1)-nMin) + nMin;
    struct lista_ciudades* ciudades = verificar_ciudades(cantidad_ciudades);
    
    int a;
    while (turno<26)
    {
        if ((jugador->deuda-jugador->gil)<=0)// ver si ya se pago la deuda
        {
            clrscr();
            printf("\n LO HAS HECHO, HAS LOGRADO PAGAR TU DEUDA \n SIGUE PELEANDO CONTRA EL CAPITALISMO QUERIDO ZACATEADOR ");
                sleep(2);

            break;
        }
        

        if(turno_jugador(jugador,ciudades,turno)){ // si la funcion devuelve 1, entonces se sale del juego
            clrscr();
            printf("Te has retirado de la batalla\n");
                sleep(2);

            scanf("%s",reglas);// No hace nada, solo es para esperar input del usuario.

            break;
        };
        

        
        

        

        turno++;
        
    }
    if ((jugador->deuda-jugador->gil)>0)// ver si ya se pago la deuda
        {
            clrscr();
            printf("\n HAS PERDIDO \n EL CAPITALISMO TRIUNFO SOBRE TI \n F");
                sleep(2);

            
        }
    


  


    return 0;
}


