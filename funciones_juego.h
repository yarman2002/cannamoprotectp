#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include<string.h>
#ifndef funciones_juego
#define funciones_juego
/*
* Cada diferente derivado de cannamo, estos se almacenaran en un arreglo
*/
struct derivado{
    char* nombre;
    int recepcion; // 1 para si, 0 para no
    int envio; //1 para si 0 para no
    int valor; //0-100
    int cantidad;
};
/*
* La lista de struct ciudad, y el int de cuantas hay
*/
struct lista_ciudades{
    struct ciudad** lista;
    int cantidad;
    
    
};

struct jugador{
    int deuda; // dinero a pagar
    int gil; //Dinero del juego
    struct derivado** tabla_derivados; //tabla de dispersion de los derivados

};
struct ciudad{
    char* nombre;
    int interes;
    struct derivado** tabla_derivados; //tabla de dispersion de los derivados
};
/*
* @param jugador . Jugador cuya tabla sera rehecha
* La funcion borra la tabla del jugador y crea otra.
*/
void* rehacer_tabla(struct jugador* jugador);
/*
* Funcion que inicializa las ciudades. Inicializa todo, y luego llama a un inicializador de tablas de derivados
* Esto ocurre solo si el interes es mayor que 30
* return : Un puntero a una lista ciudades
*/
struct lista_ciudades* crear_ciudades(int cantidad);

/*
* Funcion auxiliar a inicializar ciudades
* Les pone los nombres aleatoramente, de una lista de 10
*/
void nombrar_ciudades(struct lista_ciudades* ciudades_a_nombrar);

/*
* Funcion que imprime los datos de las ciudades bien bonito
* Llamara auxiliarmente a imprimir tabla existe una
*/
void imprimir_ciudades(struct lista_ciudades* ciudades_a_imprimir);

/*
* Funcion auxiliar. Inicializa los interes en un valor random
*/
void iniciar_interes(struct lista_ciudades* ciudades_a_inicializar);

/*
* Funcion de hash
* @param tamanno_arreglo : El tamanno arreglo
* @param llave_a_convertir : String que se convertira a una posicion
*/
int hashing_function(int tamanno_arreglo,char * llave_a_convertir);
/*
* Funcion auxiliar que crea los derivados de la tabla
*/
void crear_derivado(struct derivado* derivado_nuevo,char* nombre);
/*
* Crea la tabla de derivados en una ciudad con interes suficientes
* Funcion auxiliar
*/
struct derivado ** crear_tabla_derivados(struct ciudad* ciudad_a_poner_tabla);

/*
* Funcion que imprime las tablas de derivados bien bonito
*/
void imprimir_tabla(struct ciudad* ciudad_tabla);

/*
* Crea la tabla de derivados del jugador
*/
struct derivado ** crear_tabla_derivados_jugador(struct jugador* jugador);

/*
* Funcion que imprime las tablas de derivados del jugador bien bonito
*/
void imprimir_tabla_jugador(struct jugador* jugador);

/*
* Funcion auxiliar a hacer publicidad, sube el interes de ciudades individuales.
*/
void subir_interes(struct ciudad* ciudad_a_subir_interes);

/*
* Sube los interes de las ciudades, e imprime la subida
* Return: 0 si se hace, 1 si no se hace, por gil
*/
int hacer_publicidad(struct jugador* jugador, struct lista_ciudades* ciudades);

/*
* Funcion de vender.
* Return: 0 si se logra, 1 si no se logra.
*/
int vender(struct jugador* jugador,struct lista_ciudades* ciudades);
/*
* Funcion auxiliar a vender
* Hace la venta a una ciudad
*/
int vender_a_ciudad(struct jugador* jugador, struct ciudad* ciudad);

/*
* Funcion que se usa para recibir un string cuya longitud es desconocida
*/
char* string_indefinido();

/*
* Funcion que se usa para comprar objetos a ciudades
*/
int comprar_a_ciudad(struct jugador* jugador, struct ciudad* ciudad);
/*
* Funcion que borra tablas de derivadas, se activa si el interes baja
*/
void borrar_tabla_derivada(struct derivado** tabla_derivados);

/*
* Funcion para comerciar con ciudades, se llama desde el menu
*/
int comprar(struct jugador* jugador,struct lista_ciudades* ciudades);

/*
*  Funcion auxiliar a bajar interes
* Baja el interes a una ciudad 
*/
void bajar_inter(struct ciudad* ciudad_a_subir_interes);

/*
* Funcion que baja los interes de todas las ciudades
*/
void bajar_intereses(struct jugador* jugador, struct lista_ciudades* ciudades);
/*
* Funcion que imprime las reglas del juego
*/
void imprimir_reglas();

/*
* Funcion que baja los interes, la otra estaba bug.
*/
void foo(struct lista_ciudades* ciudades);

#endif /* funciones_juego*/